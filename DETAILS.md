# Laravel 7 Simple Referral
Sometime ago, I was asked to build a referral system as part of the feature set of a Laravel web application. Referral systems were not new to me but it was my first time building one. Referral systems are used to encourage users of an application to invite other users and get rewarded when people register for that application using their referral link.

I really wanted to keep things super simple but most of what I read online about referral systems were either too complicated or complex for my use case. After some tinkering around, I came up with something so simple I’m surprised it works. Together, we are going to build a referral system and examine some decisions and trade-offs we will have to make along the way.

A referral flow looks something like this

1. A user clicks a link that takes them to the application register page.
2. When they register, their registration should be linked to the user whose referral link they used in registering.
3. The user whose referral link was used to register should get notified that someone registered using their referral link.
4. The new user should have his or her own referral link.

We could use this scenario to model our eloquent relationships which would look like the following:
* A user can be referred by another user.
* A user can refer many users.

Now let's think about referral links and how we want our referral links to look like.
For the application I was building, one of the business requirements was that users have a referral link that could be easily remembered. Over time, I have learned that URLs that can be remembered easily are always short. There are exceptions to this but as I said, they are exceptions. After some back and forth, we agreed that referral links should look like this https://simple-referral/register?ref=username. This would require users to have a unique, unchanging username.

Rather than storing the user's referral link in the database, we are using an accessor to compute it. Then we append the attribute to the User model's array or JSON form. To read more about accessors and eloquent serialization, click [here](https://laravel.com/docs/7.x/eloquent-mutators) and [here](https://laravel.com/docs/master/eloquent-serialization).

Your requirements for a referral link might be different and here is another option you can consider https://simple-referral/register?ref=referral_token. 

### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-simple-referral.git`
2. Go inside the folder: `cd laravel-7-simple-referral`
3. Run `cp .env.example .env` then set your database credential on it.
4. Run `composer install`
5. Run `php artisan migrate`
6. Run `php artisan serve`

###  Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Registration Page

![Registration Page](img/register.png "Registration Page")

User Info Page

![User Info Page](img/info.png "User Info Page")

Registration Page

![Registration Page2](img/register2.png "Registration Page")

User Info Page

![User Info Page2](img/info2.png "User Info Page")



